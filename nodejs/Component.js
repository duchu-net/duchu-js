import PropTypes from 'prop-types'


class Component {
  static defaultProps = {}
  static propTypes = {}


  constructor(props) {
    this.props = { ...this.constructor.defaultProps, ...props }

    if (process.env.NODE_ENV === 'development') {
      PropTypes.checkPropTypes(this.constructor.propTypes, this.props, 'prop', this.constructor.name)
    }
    // this.componentWillMount()
  }

  componentWillMount() {}
  componentWillUnmount() {}
  componentDidCatch() {}
}


export default Component
