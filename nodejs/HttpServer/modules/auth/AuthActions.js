export function userLogged(user) {
  return {
    type: 'SET_CONNECTION_STATE',
    payload: user,
  };
}
