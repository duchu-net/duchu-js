import User from '../../users/UserModel';

export function register(req, res) {
  console.log('auth.controllers.register');
  // find the user
  User.findOne({ email: req.body.email }, function(err, user) {
    if (err)
      throw err;

    if (user) {
      res.status(400).json({
        success: false,
        message: 'Registration failed. User already existed.',
        errors: {
          email: 'Registration failed. User already existed.'
        }
      });
    } else if (!user) {
      const newUser = new User({
        email: req.body.email,
        password: req.body.password,
        admin: false
      });

      // save the sample user
      newUser.save(function(err) {
        if (err){
          res.json({success: false, message: err});
          return err;
        }
        // console.log(`User ${newUser.email} saved successfully`);
        res.json({ success: true, message: 'User successfully created. You can login.' });
      });
    }
  });
}
