import jwt from 'jsonwebtoken';

const APP_SECRET = 'iLoveDuchuGame';
const TOKEN_EXPIRATION = '24h';

export const sendApiResponse = (data, res, options = {}) => {
  if (options.headers) {
    [...Object.keys(options.headers)].forEach(key => {
      res.set(key, options.headers[key])
    });
  }
  return res.json({
    success: true,
    ...data
  });
}

export const sendApiErrors = (err, res, code = 400) => {
  console.log(err);
  return res.status(code).json({ errors: err.message || err });
}

export const generateToken = (email) => {
  return jwt.sign({ email }, APP_SECRET/*app.get('superSecret')*/, {
    expiresIn: TOKEN_EXPIRATION  // expires in 60min//24 hours
  });
}
