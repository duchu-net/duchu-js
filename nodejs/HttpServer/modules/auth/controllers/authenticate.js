import User from '../../users/UserModel';

export function authenticate(req, res) {
  console.log('auth.controllers.authenticate', req.body);
  const email = req.body.email;
  const password = req.body.password;

  if (!email) {
    return this.sendApiErrors({ email: 'No email provided.' }, res);
  }
  if (!password) {
    return this.sendApiErrors({ password: 'No password provided.' }, res);
  }

  let user = null;
  User.findOne({ email })
    .then(user => this.checkUserExist(user))
    .then(user => user.checkPassword(password))
    .then(user => Users.checkFirstTime(user))
    // .then(specie => Users.getPopulated(email))
    .then(user => {
      const token = this.generateToken(email);
      this.sendApiResponse({
        user: user.getPublic(),
        uid: email,
        token
      }, res, {
        headers: {
          "access-token": token,
          "uid": email
        }
      });
      return user;
    })
    .then(user => this.handleLoggedUser(user))
    .catch(err => this.sendApiErrors(err, res))
}
