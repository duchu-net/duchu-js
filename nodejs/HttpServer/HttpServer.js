import express from 'express'
import PrettyError from 'pretty-error'
import bodyParser from 'body-parser'
import helmet from 'helmet'
import http from 'http'
import cors from 'cors'
import morgan from 'morgan'
import path from 'path'
import PropTypes from 'prop-types'
import Component from '../Component'


class HttpServer extends Component {
  static defaultProps = {
    api_path: '/api',
    public_html: path.join(__dirname, './public_html'),
    host: process.env.OPENSHIFT_NODEJS_IP || process.env.NODEJS_HOST || 'localhost',
    port: process.env.OPENSHIFT_NODEJS_PORT || process.env.NODEJS_PORT || '3030',
    cors: true,
  }
  static propTypes = {
    cors: PropTypes.bool.isRequired,
    host: PropTypes.string.isRequired,
    api_path: PropTypes.string.isRequired,
    public_html: PropTypes.string.isRequired,
  }

  constructor(options = {}) {
    super(options)
    this.options = { ...this.props, ...options.http }
    // this.server = server
    if (options.api) this.options.api = options.api
  }

  init() {
    const options = this.options
    const pretty = new PrettyError()
    const app = this.app = express()
    // this.server = new http.Server(app)

    if (this.props.cors) app.use(cors())
    if (process.env.NODE_ENV !== 'production') {
      app.use(morgan('dev'))
    }
    app.use(helmet())
    app.set('superSecret', 'iloveduchu') // secret variable
    // use body parser so we can get info from POST and/or URL parameters
    app.use(bodyParser.urlencoded({ extended: false }))
    app.use(bodyParser.json())
    // SETUP SERVER SERVER ROUTES
    // console.log('Auth.authenticate', Auth.authenticate)

    // REGISTER api MAIN PATCH
    if (options.api) {
      app.use(options.api_path, options.api)
    }
    // -------------------------------------------------------------------------
    // DEFAULT ROUTE
    // app.get('/', function(req, res) {
    //     res.send('Hello! The SERVER is at http://localhost:3030/api')
    // })
    app.use(express.static(options.public_html))

    // return Promise.resolve(app)
    // return this.run()
    return app
  }

  run(server) {
    this.server = server

    const options = this.options
    if (!this.server) throw new Error('Server not specified')
    if (!options.port) throw new Error('No PORT environment variable has been specified')

    return new Promise((resolve, reject) => {
      const runnable = this.server.listen(options.port, options.host, (err) => {
        if (err) return reject(err)
        resolve()
      })
    })
  }
}

export default HttpServer
